{-|
    Module      : RTTL
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat een main-functie voor het lezen van user-supplied RTTL ringtones en het genereren van de behorende audio.
-}

module Main (main) where

import Data.Maybe (fromMaybe)
import Types (Instrument)
import Instruments (defaultInstrument , defaultSquare , defaultTriangle , pop , twisted , bass , kick , noise)
import Data (ppk, sandstorm)
import IO (playRTTL)


{-
Main function, print the instrument menu and read the number inputted by the user.
Get a RTTL string from the user.
Confirm the instrument choice 
-}
main :: IO ()
main = do putStrLn ("\nKies een instrument door een getal in te voeren")
          putStrLn instrumentMenu
          int <- fmap read getLine
          rttl <- getLine
          case chooseInstrument int of
               Just instrument -> playRTTL instrument rttl
               Nothing -> playRTTL defaultInstrument rttl             

instrumentMenu :: String
instrumentMenu = unlines [ "1: sine"
                         , "2: square"
                         , "3: triangle"
                         , "4: pop"
                         , "5: twisted"
                         , "6: bass"
                         , "7: kick"
                         , "8: noise"
                         ]

-- | Selection for types of instruments, turn an Int to a Maybe Instrument
chooseInstrument :: Int -> Maybe Instrument
chooseInstrument 2 = Just defaultSquare
chooseInstrument 3 = Just defaultTriangle
chooseInstrument 4 = Just pop
chooseInstrument 5 = Just twisted
chooseInstrument 6 = Just bass
chooseInstrument 7 = Just kick
chooseInstrument 8 = Just noise
chooseInstrument _ = Nothing
-- chooseInstrument _ = Just defaultInstrument
