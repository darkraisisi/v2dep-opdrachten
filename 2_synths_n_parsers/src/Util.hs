{-|
    Module      : Types
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat enkele algemene polymorfe functies.
-}

module Util (zipWithL, zipWithR, comb, fst3, uncurry3) where

import Control.Applicative (liftA2)

-- | Version of ZipWith guaranteed to produce a list of the same length as the second/ right input.
zipWithR :: (a -> b -> b) 
         -> [a] -- ^ The left list to zip to the right.
         -> [b] -- ^ The right list to zip to the left following this length.
         -> [b] 
zipWithR _ _      []     = []
zipWithR _ []     bs     = bs
zipWithR f (a:as) (b:bs) = f a b : zipWithR f as bs

zipWithL :: (a -> b -> a) 
         -> [a] -- ^ The left list to zip to the right following this length.
         -> [b] -- ^ The right list to zip to the left.
         -> [a]
zipWithL _ []     _     = []
zipWithL _ bs    []     = bs
zipWithL f (a:as) (b:bs) = f a b : zipWithL f as bs

-- | Use a given binary operator to combine the results of applying two separate functions to the same value. Alias of liftA2 specialised for the function applicative.
comb :: (b -> b -> b) -> (a -> b) -> (a -> b) -> a -> b
comb = liftA2

-- |  Return only the first element of a 3-tuple
fst3 :: (a, b, c) -> a
fst3 (a,_,_) = a


-- | Uncurry a function that takes a 3-tuple and make it a function that takes 3 arguments.
uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a,b,c) = f a b c
