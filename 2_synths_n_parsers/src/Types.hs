{-|
    Module      : Types
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat alle type-declaraties, instanties en type-gerelateerde functies, zodat we deze makkelijk in meerdere modules kunnen importeren.
-}

{-# LANGUAGE TypeApplications #-}

module Types ( Beats, Hz, Samples, Seconds, Semitones, Track, Ringtone
             , Tone(..), Octave(..), Duration(..), Note(..)
             , Sound, floatSound, intSound, (<+>), asFloatSound, asIntSound, getAsFloats, getAsInts
             , Instrument, instrument, Modifier, modifier, modifyInstrument, arrange
             ) where

import Data.Int (Int32)
import GHC.Float (float2Int)

type Pulse = [Float]
type Seconds = Float
type Samples = Float
type Hz = Float
type Semitones = Float
type Beats = Float
type Ringtone = String

data Tone = C | CSharp | D | DSharp | E | F | FSharp | G | GSharp | A | ASharp | B deriving (Enum, Eq, Show)
data Octave = Zero | One | Two | Three | Four | Five | Six | Seven | Eight deriving (Enum, Eq, Show)
data Duration = Full | Half | Quarter | Eighth | Sixteenth | Thirtysecond | Dotted Duration deriving (Eq, Show)
data Note = Pause Duration | Note Tone Octave Duration deriving (Eq, Show)

data Sound = IntFrames [Int32] | FloatFrames [Float]
  deriving Show

floatSound :: [Float] -> Sound
floatSound = FloatFrames

intSound :: [Int32] -> Sound
intSound = IntFrames

instance Eq Sound where
  (FloatFrames xs) == (FloatFrames ys) = all ((<  0.001) . abs) $ zipWith (-) xs ys
  (IntFrames xs) == (IntFrames ys)     = all ((< 10    ) . abs) $ zipWith (-) xs ys
  _ == _                               = False

instance Semigroup Sound where
  (FloatFrames x) <> (FloatFrames y)    = FloatFrames (x ++ y)
  (IntFrames x)   <> (IntFrames y)      = IntFrames (x ++ y)
  x               <> y                  = (asFloatSound x <> asFloatSound y)

instance Monoid Sound where
  mempty = FloatFrames[]


-- TODO Maak een operator `(<+>)` die twee `Sound`s  tot een enkel `Sound` combineert door de geluiden tegelijk af te spreken. Dit mag door de frames als in een `zipWith (+)` samen te voegen, maar in dit geval wordt niet de kortste maar de langste lijst aangehouden (in dat geval wordt enkel het aanwezige geluid weergegeven). Je hoeft deze operator alleen op `FloatFrames` te matchen, de laatste regel converteert alles hierheen als een of beide argumenten in `IntFrames` staan.

-- | Defining the mplus operator for sound.
-- | Combining two sounds together, with a preference to change inner intFramse to FloatFrames
(<+>) :: Sound -> Sound -> Sound
(FloatFrames [])      <+>   (FloatFrames [])      = (FloatFrames [])
(FloatFrames x)       <+>   (FloatFrames [])      = (FloatFrames x)
(FloatFrames [])      <+>   (FloatFrames y)       = (FloatFrames y)
FloatFrames x         <+>   FloatFrames y         = FloatFrames $ lZipWith x y 
x                     <+>   y                     = asFloatSound x <> asFloatSound y

-- | Zipping with including the longest portion of either list.
lZipWith :: [Float] -> [Float] -> [Float]
lZipWith []       []      = []
lZipWith x        []      = x
lZipWith []       y       = y
lZipWith (x:xs)   (y:ys)  =  (x + y : lZipWith xs ys)

-- | Converting an integer to a float with the same relative range as before inside a Sound.
asFloatSound :: Sound -> Sound
asFloatSound (IntFrames fs) = floatSound $ map ( (/ fromIntegral (div (maxBound @Int32 ) 2 )) . fromIntegral ) fs
asFloatSound fframe = fframe

-- | Converting a list of Floats to a list of Ints with the same relative range as before inside a Sound. 
asIntSound :: Sound -> Sound
asIntSound (FloatFrames fs) = intSound $ map (\n -> fromIntegral $ float2Int $ n * fromIntegral (div (maxBound @Int32 ) 2 ) ) fs
asIntSound fframe = fframe

getAsFloats :: Sound -> [Float]
getAsFloats sound = case asFloatSound sound of
  (FloatFrames ffs) -> ffs
  _ -> error "asFloatSound did not return FloatFrames"

getAsInts :: Sound -> [Int32]
getAsInts sound = case asIntSound sound of
  (IntFrames ifs) -> ifs
  _ -> error "asIntSound did not return IntFrames"

type Track = (Instrument, [Note])

newtype Instrument = Instrument (Hz -> Seconds -> Pulse)
{-|
Make a new instrument.

The instrument requires three arguments.
1. Hz
2. Seconds
3. A Pulse, the pulse is a list of floats, this way it can be modified.
-}
instrument :: (Hz -> Seconds -> Pulse) -> Instrument
instrument = Instrument

newtype Modifier = Modifier (Pulse -> Pulse)

modifier :: (Pulse -> Pulse) -> Modifier
modifier = Modifier

instance Semigroup Modifier where
  (Modifier m1) <> (Modifier m2) = Modifier $ m1 . m2

-- | Combine an instrument with a modifier and return an instrument
modifyInstrument :: Instrument -> Modifier -> Instrument
modifyInstrument (Instrument i) (Modifier f) = Instrument(\hz sec -> f $ i hz sec)

-- | Arrange a Sound by applying the Hz and Seconds to the array of instrument
arrange :: Instrument -> Hz -> Seconds -> Sound
arrange (Instrument i) h s = floatSound $ i h s