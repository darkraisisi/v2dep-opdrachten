{-|
    Module      : Parsers
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat parsers en functies om deze te combineren. We gebruiken simpele parsers om meer complexe parsers op te bouwen.
-}

module Parsers (Parser, parse, pComplementCharSet, pString, pOptional, pNumber, pOctave, pHeader, parse) where

import Types (Octave, Beats, Duration(..), Note(..), Tone(..))
 
import Control.Monad.Trans.State (StateT(..), evalStateT, execState, put, get)
import Data.Maybe (isJust, fromMaybe)
import Data.Char (toUpper)
import Data.List (uncons)
import Control.Monad(mzero, mplus)
import Control.Applicative ((<|>))


type Parser = (StateT String Maybe)
type CharSet = [Char]

pCharSet :: CharSet -> Parser Char
pCharSet cs = do input <- uncons <$> get
                 case input of
                   Just (h, t) -> if h `elem` cs then put t >> return h else mzero
                   Nothing -> mzero

-- | A parser that return the first character when the Char is not in the supplied list.
pComplementCharSet :: CharSet -> Parser Char
{-
pComplementCharSet cs = do input <- get
                           if head input `elem` cs 
                           then mzero 
                           else put (tail input) >> return (head input)
-}
pComplementCharSet cs = do input <- uncons <$> get
                           case input of
                            Just (h,t) -> if h `elem` cs 
                                          then mzero 
                                          else put t >> return h
                            Nothing -> mzero
                        

-- | Return a string that has been parsed if it matches a given charset in order
pString :: String -> Parser String
pString [] = return []
pString (x:xs) = do firstC <- pCharSet [x]
                    restC <- pString xs
                    return $ firstC : restC

-- | Transform a parser into an optional parser.
pOptional :: Parser a -> Parser (Maybe a)
pOptional p = do fmap Just p <|> return Nothing


pRepeatSepBy :: Parser a -> Parser b -> Parser [b]
pRepeatSepBy sep p = (:) <$> p <*> mplus (sep *> pRepeatSepBy sep p) (return [])

-- De empty parser, deze parset niets en geeft `()` terug.
pEmpty :: Parser ()
pEmpty = return ()

-- | Repeat a parse, generate a list of parsers seperated by an empty function.
pRepeat :: Parser a -> Parser [a]
pRepeat par = pRepeatSepBy pEmpty par

numbers :: CharSet
numbers = "0123456789"

-- | Parse a string of numbers to only the first set of valid numbers.
pNumber :: Parser Int
pNumber = fmap read $ pRepeat $ pCharSet numbers

pTone :: Parser Tone
pTone = do tone <- tRead . toUpper <$> pCharSet "abcdefg"
           sharp <- pOptional (pCharSet "#")
           if isJust sharp && tone `elem` [C,D,F,G,A]
             then return (succ tone)
             else return tone
  where tRead 'C' = C
        tRead 'D' = D
        tRead 'E' = E
        tRead 'F' = F
        tRead 'G' = G
        tRead 'A' = A
        tRead 'B' = B
        tRead _   = error "Invalid note"

-- | Transform a parser with numbers into an octave.
pOctave :: Parser Octave
pOctave = fmap toEnum pNumber

pDuration :: Parser Duration
pDuration = do number <- pNumber
               case number of
                 1 -> return Full
                 2 -> return Half
                 4 -> return Quarter
                 8 -> return Eighth
                 16 -> return Sixteenth
                 32 -> return Thirtysecond
                 _ -> mzero

pPause :: Duration -> Parser Note
pPause d = do duration <- fromMaybe d <$> pOptional pDuration
              _ <- pCharSet "pP"
              return $ Pause duration

pNote :: Duration -> Octave -> Parser Note
pNote d o = do duration <- fromMaybe d <$> pOptional pDuration
               tone <- pTone
               dot <- pOptional (pCharSet ".")
               octave <- fromMaybe o <$> pOptional pOctave
               return $ Note tone octave (if isJust dot then Dotted duration else duration)

pComma :: Parser ()
pComma = () <$ do _ <- pCharSet ","
                  pOptional (pCharSet " ")

-- | Parse the complete headder an return the needed information from a RTTL string.
pHeader :: Parser (String, Duration, Octave, Beats)
pHeader = do name <- pRepeat (pComplementCharSet ":")
             _ <- pCharSet ":"
             _ <- pOptional (pCharSet " ")
             _ <- pString "d="
             duration <- pDuration
             _ <- pComma
             _ <- pString "o="
             octave <- pOctave
             _ <- pComma
             _ <- pString "b="
             bpm <- fromIntegral <$> pNumber
             _ <- pCharSet ":"
             _ <- pOptional (pCharSet " ")
             return (name, duration, octave, bpm)

pSeparator :: Parser ()
pSeparator = () <$ foldl1 mplus [pString " ", pString ", ", pString ","]

pRTTL :: Parser (String, [Note], Beats)
pRTTL = do (t, d, o, b) <- pHeader
           notes <- pRepeatSepBy pSeparator $ mplus (pNote d o) (pPause d)
           return (t, notes, b)

-- | Evaluate the parsed RTTL string. 
parse :: String -> Maybe (String, [Note], Beats)
parse str = evalStateT pRTTL str